import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Link } from 'react-router-dom'
import './common.css'


class EditProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      disabled: true,
      name:localStorage.getItem("name"),
      email: localStorage.getItem("email"),
      password: localStorage.getItem("password"),
    }
  } 
  
  componentDidMount(){
    localStorage.getItem("isLoggedIn") === "true" ? this.props.history.push('/edit-profile') :this.props.history.push('/login')
  }

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleFormSubmit = () => {
    const {name, email, password, cnfPassword} = this.state
    if( name === "" || email === "" || password === "" || cnfPassword === ""){
      alert("Please fill all the fields to register.")
    } 
    else {  
            localStorage.setItem("name",name);
            localStorage.setItem("email", email);
            localStorage.setItem("password", password);
            alert("Details updated successfully!")
            window.location.reload()
          }
   }

  toggleDisable = () => {
    this.setState({disabled: false})
  }

  render() {
    return (
      <div className="App">
        <div className="card">
        <h1>Edit Profile</h1>
        <form>
          <label>
            Name : 
          <input className="textFields" type="text" name="name" placeholder="Enter your name" disabled={this.state.disabled} value={this.state.name} onChange={this.handleChange} />
          </label>
          <label>
            Email : 
          <input className="textFields" type="text" name="email" placeholder="Email" disabled={this.state.disabled} value={this.state.email} onChange={this.handleChange} />
          </label>
          <label>Password :
          <input className="textFields" type="password" name="password" placeholder="Password" disabled={this.state.disabled} value={this.state.password} onChange={this.handleChange}/>
          </label>
          {this.state.disabled ?<label>Want to edit your details?<button type="button" onClick={this.toggleDisable}> Click here </button> </label> : <button type="button" onClick={this.handleFormSubmit}>Save Changes</button>}
        </form>
            <Link to={'/login'} className="nav-link" onClick={()=>(localStorage.setItem('isLoggedIn', "false"))}>Logout</Link>
        </div>
      </div>
    );
  }

}


export default EditProfile;
