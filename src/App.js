import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {  BrowserRouter as Router, Link } from "react-router-dom";
import {Redirect, Route, Switch} from 'react-router'
import Splash from './splash'
import Login from "./login";
import Register from "./register";
import EditProfile from "./edit-profile";

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Router>
        <div>
          <Route exact path="/" component={Splash}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/edit-profile" component={EditProfile}/>
        </div>
        </Router>
    );
  }
}

export default App;
