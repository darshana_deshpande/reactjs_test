import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Link } from 'react-router-dom'
import './common.css'


class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name:'',
      email: '',
      password: '',
      cnfPassword: ''
    }
  } 
  
  componentDidMount(){
    localStorage.setItem("isLoggedIn", false)
  }
  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleFormSubmit = () => {
    const {name, email, password, cnfPassword} = this.state
    if( name === "" || email === "" || password === "" || cnfPassword === ""){
      alert("Please fill all the fields to register.")
    } 
    else {
      if(password !== cnfPassword) {
        alert("Passwords donot match. Please try again.")
      }
      else {
        const r_email = localStorage.getItem("email");
        if(r_email !== null){
          if(email === r_email) {
            alert('This email is already registered with us. Register with a new email or proceed by logging in.')
          } else {
            localStorage.setItem("name",name);
            localStorage.setItem("email", email);
            localStorage.setItem("password", password);
            alert("Registration Successful!")
            this.props.history.push('/login')
          }
        }
      }
    }
  }

  render(){
    return (
      <div className="App">
        <div className="card">
        <h1>Register</h1>
        <form>
          <input className="textFields"  type="text" name="name" placeholder="Enter your name" value={this.state.name} onChange={this.handleChange} required />
          <input className="textFields" type="text" name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange} required/>
          <input className="textFields" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} required/>
          <input className="textFields" type="password" name="cnfPassword" placeholder="Confirm Password" value={this.state.cnfPassword} onChange={this.handleChange} required/>
          <button type="button" onClick={this.handleFormSubmit}>REGISTER</button>
        </form>
           
        <label>Already a registered user?</label><Link to={'/login'} className="nav-link">Login</Link>
        </div>
      </div>
    );
  }
}



export default Register;
