import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {  BrowserRouter as Router, Link } from "react-router-dom";
import {Redirect, Route, Switch} from 'react-router'
import Login from "./login";
import Register from "./register";
import EditProfile from "./edit-profile";

class Splash extends React.Component {
  constructor(props) {
    super(props);
    this.state = { redirect: false };
  }

  componentDidMount() {
   
    setTimeout(() => this.setState({ redirect: true }), 2000);
  }

  render() {
    return (
      <div className="App">
        {this.state.redirect === false ? (
          <header className="App-header">
            <h1>React.js Demo Application!</h1>
            <p>(This is a splashscreen!)</p>
          </header>
        ) : 
      
           <Redirect to="/login"/>
      }
      </div>
    );
  }
}

export default Splash;
