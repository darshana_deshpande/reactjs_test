import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Link} from 'react-router-dom';
import { Redirect } from 'react-router';
import './common.css'


class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  } 
  
  componentDidMount(){
    localStorage.setItem("isLoggedIn", false)
  }

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleFormSubmit = () => {
    const r_email = localStorage.getItem("email");
    const r_password = localStorage.getItem("password");
    if (r_email === null && r_password === null){
      alert("Seems like you aren't a registered user. Kindly register to continue.")
    }
    else {
      if(this.state.email !== r_email || this.state.password !== r_password){
        alert("Invalid username or password. Please try again.")
      }
      else{
        localStorage.setItem('isLoggedIn', "true")
        this.props.history.push('/edit-profile')
      }
    }
  }
 
  render(){
    return (
      <div className="App">
        <div className="card">
        <h1>Login</h1>
        <form>
          <input class="textFields" type="text" name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange} required />
          <input class="textFields" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} required/>
          <button type="button" onClick={this.handleFormSubmit}>LOGIN</button>
        </form>
            {/* <input type="submit" value="Submit" onClick={this.handleFormSubmit}/> */}
           
            
            <label>Not a registered user? </label><Link to={'/register'} className="nav-link">Register Now</Link>
            </div>
      </div>
    );
  }
}



export default Login;
